﻿namespace Assets.Scripts
{
    public interface IZoom
    {
        float CurrentZoom();
        void ZoomIn();
        void ZoomOut();
    }
}