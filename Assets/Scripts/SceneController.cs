﻿using Assets.Scripts.GameLogic;
using Assets.Scripts.GameLogic.PlanetManager;
using Assets.Scripts.Input;
using Assets.Scripts.SpaceItems;
using Assets.Scripts.View;
using UnityEngine;

namespace Assets.Scripts
{
    public class SceneController : MonoBehaviour
    {
        public SpaceShip SpaceShipPrefab;
        public BaseInputHandler InputHandler;
        public PlanetManager PlanetManager;
        public ViewController ViewController;
        public VisibleAreaController VisibleAreaController;

        private SpaceShip _spaceShipInstance;

        private readonly float _spaceShipStep = 1f;

        void Awake()
        {
            InputHandler = new InputHandler();
            InputHandler.OnUpButtonPressed += OnUpButtonPressed;
            InputHandler.OnDownButtonPressed += OnDownButtonPressed;
            InputHandler.OnLeftButtonPressed += OnLeftButtonPressed;
            InputHandler.OnRightButtonPressed += OnRightButtonPressed;
            InputHandler.OnPlusButtonPressed += VisibleAreaController.OnZoomIn;
            InputHandler.OnMinusButtonPressed += VisibleAreaController.OnZoomOut;
            InputHandler.OnPlusButtonPressed += ViewController.ZoomIn;
            InputHandler.OnMinusButtonPressed += ViewController.ZoomOut;

            
            _spaceShipInstance = Instantiate(SpaceShipPrefab, Vector2.zero, Quaternion.identity);
            _spaceShipInstance.OnPositionChanged += VisibleAreaController.OnSpaceShipPositionChanged;

            VisibleAreaController.OnVisibleAreaChanged += ViewController.OnVisibleAreaChanged;

            PlanetManager.SpaseShipRating = _spaceShipInstance.Rating;
        }
        
        void Update()
        {
            if (InputHandler != null)
                InputHandler.Update();
        }

        private void OnUpButtonPressed()
        {
            _spaceShipInstance.Move(new Vector2(0, _spaceShipStep));
        }
        
        private void OnDownButtonPressed()
        {
            _spaceShipInstance.Move(new Vector2(0, -_spaceShipStep));
        }
        
        private void OnLeftButtonPressed()
        {
            _spaceShipInstance.Move(new Vector2(-_spaceShipStep, 0));
        }
        
        private void OnRightButtonPressed()
        {
            _spaceShipInstance.Move(new Vector2(_spaceShipStep, 0));
        }
    }
}