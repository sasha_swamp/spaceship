﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Zoom : IZoom
    {
        private float _zoom;
        private readonly int _minZoom = 5;
        private readonly int _maxZoom = 10000;
        private readonly float _zoomStep = 1f; 

        public Zoom(float zoom)
        {
            _zoom = Mathf.Clamp(zoom, _minZoom, _maxZoom);
        }
        
        public float CurrentZoom()
        {
            return _zoom;
        }

        public void ZoomIn()
        {
            _zoom -= _zoomStep;
            if (_zoom >= _maxZoom)
                _zoom = _maxZoom;
        }

        public void ZoomOut()
        {
            _zoom += _zoomStep;
            if (_zoom <= _minZoom)
                _zoom = _minZoom;
        }
    }
}