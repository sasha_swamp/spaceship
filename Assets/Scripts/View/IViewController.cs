﻿namespace Assets.Scripts.View
{
    public interface IViewController
    {
        void ZoomIn();
        void ZoomOut();
    }
}