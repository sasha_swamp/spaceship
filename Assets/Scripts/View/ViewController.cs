﻿using UnityEngine;

namespace Assets.Scripts.View
{
    public class ViewController : MonoBehaviour, IViewController
    {
        public Camera Camera;

        private readonly float _zoomStep = 1f;

        void Awake()
        {
            transform.Translate(Vector3.zero);
        }

        public void OnVisibleAreaChanged(Zone zone)
        {
            if (zone == null)
                return;
            
            float x = (zone.BottomRight.x + zone.TopLeft.x) / 2;
            float y = (zone.TopLeft.y + zone.BottomRight.y) / 2;
            float z = Camera.transform.position.z;
            Camera.transform.position = new Vector3(x, y, z);
        }

        public void ZoomIn()
        {
            if (Camera.orthographicSize <= 1f)
                return;
            
            Camera.orthographicSize -= _zoomStep;
        }
        
        public void ZoomOut()
        {
            if (Camera.orthographicSize >= 50)
                return;
            
            Camera.orthographicSize += _zoomStep;
        }
       
    }
}