﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class Zone
    {
        public Vector2 TopLeft;
        public Vector2 BottomRight;


        public Zone(Vector2 topLeft, Vector2 bottomRight)
        {
            TopLeft = topLeft;
            BottomRight = bottomRight;
        }

        public bool ContainsPosition(Vector2 position)
        {
            if (position.x < TopLeft.x || position.x > BottomRight.x)
                return false;
            
            if (position.y < BottomRight.y || position.y > TopLeft.y)
                return false;

            return true;
        }

        // Пока учитываем только горизонтальные и вертикальные перемещения.
        public List<Zone> Difference(Zone zone)
        {
            List<Zone> ret = new List<Zone>();
            
            if (zone == null)
                return ret;
            
            // Проверяем - если входная зона содержится внутри текущей, тогда разницы нету.
            if (IsZoneInside(zone))
            {
                return ret;
            }

            // Определяем с какой стороны у нас торчат зоны, которые являются результатом.
            // Нет учёта того, что итоговые зоны могут пересекаться на углах.
            
            // Влево.
            if (zone.TopLeft.x < this.TopLeft.x)
            {
                Vector2 topLeft = new Vector2(zone.TopLeft.x, zone.TopLeft.y);
                Vector2 bottomRight = new Vector2(this.TopLeft.x, this.BottomRight.y);
                ret.Add(new Zone(topLeft, bottomRight));
            }
            // Вверх.
            if (zone.TopLeft.y > this.TopLeft.y)
            {
                Vector2 topLeft = new Vector2(zone.TopLeft.x, zone.TopLeft.y);
                Vector2 bottomRight = new Vector2(this.BottomRight.x, this.TopLeft.y);
                ret.Add(new Zone(topLeft, bottomRight));
            }
            // Вправо.
            if (zone.BottomRight.x > this.BottomRight.x)
            {
                Vector2 topLeft = new Vector2(this.BottomRight.x, zone.TopLeft.y);
                Vector2 bottomRight = new Vector2(zone.BottomRight.x, zone.BottomRight.y);
                ret.Add(new Zone(topLeft, bottomRight));
            }
            // Вниз.
            if (zone.BottomRight.y < this.BottomRight.y)
            {
                Vector2 topLeft = new Vector2(zone.TopLeft.x, this.BottomRight.y);
                Vector2 bottomRight = new Vector2(zone.BottomRight.x, zone.BottomRight.y);
                ret.Add(new Zone(topLeft, bottomRight));
            }

            return ret;
        }

        private bool IsZoneInside(Zone zone)
        {
            if (zone == null)
                return false;

            return (zone.TopLeft.x     >= this.TopLeft.x &&
                    zone.BottomRight.x <= this.BottomRight.x &&
                    zone.TopLeft.y     <= this.TopLeft.y &&
                    zone.BottomRight.y >= this.BottomRight.y);
        }
    }
}