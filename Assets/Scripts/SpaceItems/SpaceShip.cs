﻿using UnityEngine;

namespace Assets.Scripts.SpaceItems
{
    public class SpaceShip : BaseSpaceItem
    {
        public delegate void PositionChanged(Vector2 position);
        public event PositionChanged OnPositionChanged;

        public override Vector2 Position()
        {
            return transform.position;
        }
        
        public void Move(Vector2 delta)
        {
            transform.Translate(delta);
            if (OnPositionChanged != null) 
                OnPositionChanged(transform.position);
        }
        
        public void SetPosition(Vector2 newPosition)
        {
            transform.position = newPosition;
            if (OnPositionChanged != null) 
                OnPositionChanged(newPosition);
        }
    }
}