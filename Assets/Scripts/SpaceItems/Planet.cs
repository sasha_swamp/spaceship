﻿using UnityEngine;

namespace Assets.Scripts.SpaceItems
{
    public class Planet : BaseSpaceItem
    {
        public override Vector2 Position()
        {
            return transform.position;
        }
    }
}