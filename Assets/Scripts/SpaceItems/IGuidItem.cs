﻿using System;

namespace Assets.Scripts.SpaceItems
{
    public interface IGuidItem
    {
        Guid Id { get; set; }
    }
}