﻿using System;
using UnityEngine;

namespace Assets.Scripts.SpaceItems
{
    public abstract class BaseSpaceItem : MonoBehaviour, IGuidItem
    {
        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public int Rating;
       
        protected BaseSpaceItem()
        {
            Id = Guid.NewGuid();
            Rating = 100;
        }
        
        public abstract Vector2 Position();
    }
}