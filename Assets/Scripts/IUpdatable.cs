﻿namespace Assets.Scripts
{
    public interface IUpdatable
    {
        void Update();
    }
}