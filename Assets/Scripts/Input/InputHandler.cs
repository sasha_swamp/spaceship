﻿using UnityEngine;

namespace Assets.Scripts.Input
{
    public class InputHandler : BaseInputHandler
    {
        public override void Update()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.W))
            {
                base.UpButtonPressed();
            }
            else if (UnityEngine.Input.GetKeyDown(KeyCode.S))
            {
                base.DownButtonPressed();
            }
            else if (UnityEngine.Input.GetKeyDown(KeyCode.A))
            {
                base.LeftButtonPressed();
            }
            else if (UnityEngine.Input.GetKeyDown(KeyCode.D))
            {
                base.RightButtonPressed();
            }
            else if (UnityEngine.Input.GetKeyDown(KeyCode.Z))
            {
                base.PlusButtonPressed();
            }
            else if (UnityEngine.Input.GetKeyDown(KeyCode.X))
            {
                base.MinusButtonPressed();
            }
        }
    }
}