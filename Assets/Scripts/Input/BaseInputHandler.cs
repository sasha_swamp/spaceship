﻿namespace Assets.Scripts.Input
{
    public abstract class BaseInputHandler : IUpdatable
    {
        public delegate void InputAction();

        public event InputAction OnUpButtonPressed;
        public event InputAction OnDownButtonPressed;
        public event InputAction OnLeftButtonPressed;
        public event InputAction OnRightButtonPressed;
        public event InputAction OnPlusButtonPressed;
        public event InputAction OnMinusButtonPressed;

        public abstract void Update();

        public void UpButtonPressed()
        {
            if (OnUpButtonPressed != null) 
                OnUpButtonPressed.Invoke();
        }
        
        public void DownButtonPressed()
        {
            if (OnDownButtonPressed != null) 
                OnDownButtonPressed.Invoke();
        }
        
        public void LeftButtonPressed()
        {
            if (OnLeftButtonPressed != null) 
                OnLeftButtonPressed.Invoke();
        }
        
        public void RightButtonPressed()
        {
            if (OnRightButtonPressed != null) 
                OnRightButtonPressed.Invoke();
        }
        
        public void PlusButtonPressed()
        {
            if (OnPlusButtonPressed != null) 
                OnPlusButtonPressed.Invoke();
        }
        
        public void MinusButtonPressed()
        {
            if (OnMinusButtonPressed != null) 
                OnMinusButtonPressed.Invoke();
        }
    }
}