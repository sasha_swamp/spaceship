﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameLogic.PlanetManager
{
    public class PlanetInfoCache : ElementsCache<PlanetInfo>
    {
        public List<PlanetInfo> PlanetInfosInsideZone(Zone zone)
        {
            List<PlanetInfo> ret = new List<PlanetInfo>();

            foreach (var planetInfo in _elements)
            {
                if (planetInfo == null)
                    continue;

                if (!zone.ContainsPosition(planetInfo.Position))
                    continue;
                
                ret.Add(planetInfo);
            }
            
            return ret;
        }
        
        public List<PlanetInfo> PlanetInfosOutsideZone(Zone zone)
        {
            List<PlanetInfo> ret = new List<PlanetInfo>();

            foreach (var planetInfo in _elements)
            {
                if (planetInfo == null)
                    continue;

                if (zone.ContainsPosition(planetInfo.Position))
                    continue;
                
                ret.Add(planetInfo);
            }
            
            return ret;
        }
    }
}