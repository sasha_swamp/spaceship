﻿using System;
using Assets.Scripts.SpaceItems;
using UnityEngine;

namespace Assets.Scripts.GameLogic.PlanetManager
{
    public class PlanetInfo : IGuidItem
    {
        private Guid _id;

        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public Vector2 Position;
        public int Rating;
        
        public PlanetInfo(Guid id, Vector2 position, int rating)
        {
            _id = id;
            Position = position;
            Rating = rating;
        }
    }
}