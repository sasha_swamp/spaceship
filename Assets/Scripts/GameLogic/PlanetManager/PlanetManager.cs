﻿using System.Collections.Generic;
using Assets.Scripts.GameLogic.PlanetManager.PlanetsSelector;
using Assets.Scripts.SpaceItems;
using UnityEngine;

namespace Assets.Scripts.GameLogic.PlanetManager
{
    public class PlanetManager : MonoBehaviour
    {
        public VisibleAreaController VisibleAreaController;
        public PlanetGenerator.PlanetGenerator PlanetGenerator;
        public int SpaseShipRating;
        
        private ElementsCache<Planet> _planets;
        private IPlanetsSelector _planetSelector;
        private IPlanetCacheProvider _planetCacheProvider;
        private PlanetInfoCache _planetsInfoCache;
        private VisitedArea _visitedArea;
        
        private Zone _lastVisibleZone;
        private readonly float _zoomValueSpecialMode = 10f;

        void Awake()
        {
            VisibleAreaController.OnVisibleAreaChanged += OnVisibleAreaChanged;
            
            _planets = new ElementsCache<Planet>();
            _planetCacheProvider = new PlanetCacheProvider();
            _planetsInfoCache = new PlanetInfoCache();

            _lastVisibleZone = new Zone(VisibleAreaController.TopLeft, VisibleAreaController.BottomRight);
            _visitedArea = new VisitedArea(_lastVisibleZone);

            _planetsInfoCache.OnCacheFilled += OnPlanetCacheFilled;
            LoadCache(_planetsInfoCache, _visitedArea);
        }

        void Start()
        {
            _planetSelector = new PlanetsSelector.PlanetsSelector(SpaseShipRating);
        }
        
        
        public void OnVisibleAreaChanged(Zone zone)
        {
            Zone lastVisitedArea = new Zone(_visitedArea.VisitedZone.TopLeft, _visitedArea.VisitedZone.BottomRight);
            
            // Расширяем зону где были
            _visitedArea.Expand(zone);

            // Пока очищаем полностью пул планет. А вообще надо обсчитывать пересечение областей видимости, убивать только
            // то, что пропало и добавлять только в зону разницы между предыдущей зоной видимости и текущей.
            foreach (var planet in _planets.Elements())
            {
                if (planet == null)
                    continue;
                
                Destroy(planet.gameObject);
            }
            _planets.Clear();

            // Получаем разницу между прошлой посещённой зоной и текущей.
            List<Zone> diffZones = lastVisitedArea.Difference(_visitedArea.VisitedZone);
            
            // Генерим новые данные по планетам после расширения посещённой зоны.
            foreach (var diffZone in diffZones)
            {
                foreach (var planetInfo in PlanetGenerator.GeneratePlanetInfos(diffZone))
                {
                    _planetsInfoCache.Add(planetInfo);
                }
            }

            if (VisibleAreaController.Zoom.CurrentZoom() < _zoomValueSpecialMode)
            {
                // Добааляем из кэша на сцену все планеты, которые попали в зону видимости. 
                foreach (var planetInfo in _planetsInfoCache.PlanetInfosInsideZone(zone))
                {
                    Planet planet = PlanetGenerator.InstantiatePlanet(planetInfo);
                    _planets.Add(planet);
                }
            }
            else
            {
                foreach (var planetInfo in _planetSelector.SelectPlanets(_planetsInfoCache, zone))
                {
                    Planet planet = PlanetGenerator.InstantiatePlanet(planetInfo);
                    _planets.Add(planet);
                }
            }
        }

        public void OnPlanetCacheFilled()
        {
            _planetCacheProvider.Save(_planetsInfoCache, _visitedArea);
            
            // Хитро очищаем кэш.
        }

        private void LoadCache(PlanetInfoCache planetInfoCache, VisitedArea visitedArea)
        {
            // Если кэш не загрузился, то генерим сами новые планеты в кэш.
            if (!_planetCacheProvider.Load(_planetsInfoCache, _visitedArea))
            {
                foreach (var planet in PlanetGenerator.GeneratePlanets(_lastVisibleZone))
                {
                    _planets.Add(planet);
                    PlanetInfo planetInfo = new PlanetInfo(planet.Id, planet.Position(), planet.Rating);
                    _planetsInfoCache.Add(planetInfo);
                }                
            }
            else
            {
                foreach (var planetInfo in _planetsInfoCache.PlanetInfosInsideZone(_lastVisibleZone))
                {
                    _planets.Add(PlanetGenerator.InstantiatePlanet(planetInfo));
                }
            }
        }
    }
}