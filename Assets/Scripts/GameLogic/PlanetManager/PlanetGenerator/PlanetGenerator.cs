﻿using System;
using System.Collections.Generic;
using Assets.Scripts.SpaceItems;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.GameLogic.PlanetManager.PlanetGenerator
{
    public class PlanetGenerator : MonoBehaviour, IPlanetGenerator
    {
        public Planet PlanetPrefab;
        public float Probability;

        private readonly int _minRating = 0;
        private readonly int _maxRating = 10000;
        
        public Planet InstantiatePlanet(PlanetInfo planetInfo)
        {
            Planet ret = Instantiate(PlanetPrefab, planetInfo.Position, Quaternion.identity);
            ret.Id = planetInfo.Id;
            ret.Rating = planetInfo.Rating;

            return ret;
        }
        
        public List<Planet> GeneratePlanets(Zone zone)
        {
            List<Planet> ret = new List<Planet>();
            if (zone == null)
                return ret;

            foreach (var planetInfo in GeneratePlanetInfos(zone))
                ret.Add(InstantiatePlanet(planetInfo));

            return ret;
        }

        public List<PlanetInfo> GeneratePlanetInfos(Zone zone)
        {
            List<PlanetInfo> ret = new List<PlanetInfo>();
            if (zone == null)
                return ret;
            
            int cellsCount = (int)Mathf.Round(zone.BottomRight.x - zone.TopLeft.x) * (int)Mathf.Round(zone.TopLeft.y - zone.BottomRight.y);
            for (int i = 0; i < cellsCount * Probability; ++i)
            {
                float x = Mathf.Round(Random.Range(zone.TopLeft.x, zone.BottomRight.x));
                float y = Mathf.Round(Random.Range(zone.BottomRight.y, zone.TopLeft.y));

                int rating = Random.Range(_minRating, _maxRating);

                PlanetInfo planetInfo = new PlanetInfo(Guid.NewGuid(), new Vector2(x, y), rating);
                
                ret.Add(planetInfo);
            }

            return ret;
        }
    }
}