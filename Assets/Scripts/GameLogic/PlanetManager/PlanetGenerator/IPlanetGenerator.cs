﻿using System.Collections.Generic;
using Assets.Scripts.SpaceItems;

namespace Assets.Scripts.GameLogic.PlanetManager.PlanetGenerator
{
    public interface IPlanetGenerator
    {
        Planet InstantiatePlanet(PlanetInfo planetInfo);
        List<Planet> GeneratePlanets(Zone zone);
        List<PlanetInfo> GeneratePlanetInfos(Zone zone);
    }
}