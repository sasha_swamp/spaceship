﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.GameLogic.PlanetManager.PlanetsSelector
{
    public class PlanetsSelector : IPlanetsSelector
    {
        public int SpaceShipRating;

        private readonly int _planetsCount = 20;

        public PlanetsSelector(int spaceShipRating)
        {
            SpaceShipRating = spaceShipRating;
        }
        
        public List<PlanetInfo> SelectPlanets(PlanetInfoCache planetInfoCache, Zone zone)
        {
            List<PlanetInfo> ret = new List<PlanetInfo>();
            if (planetInfoCache == null || zone == null)
                return ret;

            // Решение в лоб, но зато рабочее. Надо оптимизировать поиск планет
            List<KeyValuePair<PlanetInfo, int>> planetInfosRatingDiffs = new List<KeyValuePair<PlanetInfo, int>>();
            
            // Выбираем все планеты видимой области из кэша, считаем разницу рейтинга с кораблём
            // и запоминаем это.
            foreach (var planetInfo in planetInfoCache.PlanetInfosInsideZone(zone))
            {
                if (planetInfo == null)
                    continue;

                int diff = Math.Abs(SpaceShipRating - planetInfo.Rating);
                planetInfosRatingDiffs.Add(new KeyValuePair<PlanetInfo, int>(planetInfo, diff));
            }
            
            // Сортируем массив по возрастанию разниц рейтингов
            planetInfosRatingDiffs.Sort((info1, info2) => info1.Value.CompareTo(info2.Value));
            
            // Выбираем с начала списка столько планет, сколько надо.  
            for (int i = 0; i < _planetsCount; ++i)
            {
                var planetInfoDiff = planetInfosRatingDiffs[i];
                ret.Add(planetInfoDiff.Key);
            }
            
            return ret;
        }
    }
}