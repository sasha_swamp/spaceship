﻿using System.Collections.Generic;

namespace Assets.Scripts.GameLogic.PlanetManager.PlanetsSelector
{
    public interface IPlanetsSelector
    {
        List<PlanetInfo> SelectPlanets(PlanetInfoCache planetInfoCache, Zone zone);
    }
}