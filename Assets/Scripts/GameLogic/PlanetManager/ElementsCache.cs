﻿using System;
using System.Collections.Generic;
using Assets.Scripts.SpaceItems;

namespace Assets.Scripts.GameLogic.PlanetManager
{
    public class ElementsCache<T>
        where T: IGuidItem
    {
        public delegate void CacheFilled();
        public event CacheFilled OnCacheFilled;   
        
        // Use Hashtable for fast lookup.
        protected List<T> _elements;
        private readonly int _maxCachedElementsCount = 100000;
        
        public ElementsCache()
        {
            _elements = new List<T>();
        }

        public void Add(T element)
        {
            _elements.Add(element);
            
            // Отправляем сигнал о заполнении кэша. 
            if (_elements.Count == _maxCachedElementsCount)
                if (OnCacheFilled != null)
                    OnCacheFilled.Invoke();
        }
        
        public void Remove(Guid id)
        {
            for (int i = 0; i < _elements.Count; ++i)
            {
                T element = _elements[i];
                if (element == null)
                    continue;

                if (element.Id != id)
                    continue;
                
                _elements.RemoveAt(i);
                return;
            }
        }
        
        public List<T> Elements()
        {
            return _elements;
        }

        public void Clear()
        {
            _elements.Clear();
        }
    }
}