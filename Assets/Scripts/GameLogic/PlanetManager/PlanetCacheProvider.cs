﻿namespace Assets.Scripts.GameLogic.PlanetManager
{
    public class PlanetCacheProvider : IPlanetCacheProvider
    {
        public void Save(PlanetInfoCache planetInfoCache, VisitedArea visitedArea)
        {
            if (planetInfoCache == null || visitedArea == null)
                return;
            
            // Тут реализовываем сохранение кэша и посещенной области.
        }

        public bool Load(PlanetInfoCache planetInfoCache, VisitedArea visitedArea)
        {
            if (planetInfoCache == null || visitedArea == null)
                return false;
            
            // Тут реализовываем загрузку кэша и посещенной области.
            return false;
        }
    }
}