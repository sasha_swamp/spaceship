﻿using UnityEngine;

namespace Assets.Scripts.GameLogic
{
    public class VisibleAreaController : MonoBehaviour
    {
        public delegate void VisibleAreaChanged(Zone zone);
        public event VisibleAreaChanged OnVisibleAreaChanged;
        
        public IZoom Zoom;
        public Vector2 TopLeft;
        public Vector2 BottomRight;

        private readonly float _startZoomValue = 7f;

        void Awake()
        {
            Zoom = new Zoom(_startZoomValue);
            TopLeft = new Vector2(Mathf.Round(-_startZoomValue / 2), Mathf.Round(_startZoomValue / 2));
            BottomRight = new Vector2(Mathf.Round(_startZoomValue / 2), Mathf.Round(-_startZoomValue / 2));
        }

        void Start()
        {
            OnSpaceShipPositionChanged(Vector2.zero);
        }
        
        public void OnSpaceShipPositionChanged(Vector2 position)
        {
            TopLeft = TopLeftByCenter(position.x, position.y);
            BottomRight = BottomRightByCenter(position.x, position.y);
            if (OnVisibleAreaChanged != null)
                OnVisibleAreaChanged.Invoke(new Zone(TopLeft, BottomRight));
        }

        public void OnZoomIn()
        {
            Zoom.ZoomIn();
            
            OnZoomChanged();
        }
        
        public void OnZoomOut()
        {
            Zoom.ZoomOut();
            
            OnZoomChanged();
        }

        private void OnZoomChanged()
        {
            float centerX = (BottomRight.x + TopLeft.x) / 2;
            float centerY = (TopLeft.y + BottomRight.y) / 2;
            
            TopLeft = TopLeftByCenter(centerX, centerY);
            BottomRight = BottomRightByCenter(centerX, centerY);
            
            if (OnVisibleAreaChanged != null)
                OnVisibleAreaChanged.Invoke(new Zone(TopLeft, BottomRight));
        }

        private Vector2 TopLeftByCenter(float centerX, float centerY)
        {
            return new Vector2(Mathf.Round(centerX - Zoom.CurrentZoom() / 2), Mathf.Round(centerY + Zoom.CurrentZoom() / 2));
        }
        
        private Vector2 BottomRightByCenter(float centerX, float centerY)
        {
            return new Vector2(Mathf.Round(centerX + Zoom.CurrentZoom() / 2), Mathf.Round(centerY - Zoom.CurrentZoom() / 2));
        }
    }
}