﻿using Assets.Scripts.GameLogic.PlanetManager;

namespace Assets.Scripts.GameLogic
{
    public interface IPlanetCacheProvider
    {
        void Save(PlanetInfoCache planetInfoCache, VisitedArea visitedArea);
        bool Load(PlanetInfoCache planetInfoCache, VisitedArea visitedArea);
    }
}