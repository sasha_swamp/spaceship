﻿using System;
using UnityEngine;

namespace Assets.Scripts.GameLogic
{
    public class VisitedArea
    {
        public Zone VisitedZone;

        public VisitedArea(Zone zone)
        {
            VisitedZone = zone;
        }

        public void Expand(Zone zone)
        {
            if (zone == null)
                return;

            float topLeftX = Math.Min(VisitedZone.TopLeft.x, zone.TopLeft.x);
            float topLeftY = Math.Max(VisitedZone.TopLeft.y, zone.TopLeft.y);
            float bottomRightX = Math.Max(VisitedZone.BottomRight.x, zone.BottomRight.x);
            float bottomRightY = Math.Min(VisitedZone.BottomRight.y, zone.BottomRight.y);
            
            VisitedZone = new Zone(new Vector2(topLeftX, topLeftY), new Vector2(bottomRightX, bottomRightY));
        }
    }
}